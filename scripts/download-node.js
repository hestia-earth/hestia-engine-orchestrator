require("dotenv").config();
const fs = require("fs");
const { join, resolve } = require("path");
const { S3 } = require("aws-sdk");
const { DataState, pathWithState } = require("@hestia-earth/api");
const { writeFile } = fs.promises;

const [type, id, dataState] = process.argv.slice(2);
const bucket = process.env.AWS_BUCKET;
const ROOT = resolve(join(__dirname, "../"));

const s3 = new S3();

const downloadCache = async (nodeType, nodeId) => {
  try {
    const key = [nodeType, `${nodeId}.cache`].join("/");
    const { Body } = await s3.getObject({ Bucket: bucket, Key: key }).promise();
    console.log("Trying to download", key);
    return JSON.parse(Body.toString());
  } catch (err) {
    return null;
  }
};

const downloadState = async (nodeType, nodeId, state) => {
  const key = pathWithState(nodeType, nodeId, state);
  const { Body } = await s3.getObject({ Bucket: bucket, Key: key }).promise();
  console.log("Trying to download", key);
  return JSON.parse(Body.toString());
};

const downloadNode = async (nodeType, nodeId, dataState) => {
  try {
    const node = await downloadState(nodeType, nodeId, dataState);
    const _cache = await downloadCache(nodeType, nodeId);
    return {
      ...node,
      ...(_cache ? { _cache } : {}),
    };
  } catch (err) {
    console.log("Failed to download");
    return dataState === DataState.recalculated
      ? await downloadState(nodeType, nodeId, DataState.original)
      : null;
  }
};

const run = async () => {
  const node = await downloadNode(
    type,
    id,
    dataState || DataState.recalculated
  );
  if ("cycle" in node) {
    node.cycle = await downloadNode(
      node.cycle["@type"],
      node.cycle["@id"],
      DataState.recalculated
    );
    if ("site" in node.cycle) {
      node.cycle.site = await downloadNode(
        node.cycle.site["@type"],
        node.cycle.site["@id"],
        DataState.recalculated
      );
    }
  }
  if ("site" in node) {
    node.site = await downloadNode(
      node.site["@type"],
      node.site["@id"],
      DataState.recalculated
    );
  }
  return node
    ? await writeFile(
        join(ROOT, "samples", `${id}.jsonld`),
        JSON.stringify(node, null, 2),
        "utf-8"
      )
    : null;
};

run()
  .then(() => process.exit(0))
  .catch((err) => {
    console.error(err);
    process.exit(1);
  });
