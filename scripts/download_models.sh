#!/bin/sh

API_URL=${1:-"https://api.hestia.earth"}
DATA_DIR=${2:-"/usr/local/lib/python3.7/site-packages/"}

mkdir -p scripts
cd scripts

curl https://gitlab.com/hestia-earth/hestia-engine-models/-/raw/develop/scripts/download_impact_assessments.py?inline=false -o download_impact_assessments.py
curl https://gitlab.com/hestia-earth/hestia-engine-models/-/raw/develop/scripts/download_data.sh?inline=false -o download.sh
chmod +x download.sh

cd ../

./scripts/download.sh $API_URL $DATA_DIR
rm scripts/download.sh
