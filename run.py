# usage:
# 1. Install python-dotenv
# 2. Set your env variables in a `.env` file
# 3. Put your test cycle into the `samples` folder
# 3. Run `python run.py samples/cycle.jsonld`
from dotenv import load_dotenv
load_dotenv()


import argparse
import sys
import logging
import json
from hestia_earth.orchestrator import run
from hestia_earth.orchestrator.log import logger
from hestia_earth.config import load_config
from hestia_earth.models.log import logger as models_logger
from hestia_earth.models.preload_requests import enable_preload

parser = argparse.ArgumentParser()
parser.add_argument('--filepath', type=str, required=True,
                    help='Path of the file containing the node to recalculate.')
parser.add_argument('--config-path', type=str,
                    help='Path of the configuration file to use. Use HESTIA default if not specified.')
parser.add_argument('--stage', type=int,
                    help='Filter models by stage. Run all models if not specified.')
args = parser.parse_args()


def _init_gee(node: dict):
    node_type = node.get('@type', node.get('type'))
    if node_type == 'Site':
        try:
            from hestia_earth.earth_engine import init_gee
        except ImportError:
            raise ImportError("Run `pip install hestia_earth.earth_engine` to use this functionality")

        init_gee()

        from hestia_earth.earth_engine.log import logger as ee_logger

        ee_logger.addHandler(logging.StreamHandler(sys.stdout))
        ee_logger.setLevel(logging.getLevelName('DEBUG'))


def main():
    # print everything to console as disabled by default
    logger.addHandler(logging.StreamHandler(sys.stdout))
    logger.setLevel(logging.getLevelName('DEBUG'))

    models_logger.addHandler(logging.StreamHandler(sys.stdout))
    models_logger.setLevel(logging.getLevelName('DEBUG'))

    with open(args.filepath) as f:
        node = json.load(f)

    _init_gee(node)

    enable_preload(filepath='search-results.json', node=node, overwrite_existing=False)

    print(f"processing {args.filepath}")

    config = args.config_path or load_config(data.get('@type', data.get('type')))
    data = run(node, config)

    path, ext = args.filepath.split('.')

    with open(f"{path}-processed.{ext}", 'w') as f:
        f.write(json.dumps(data, indent=2, ensure_ascii=False))


if __name__ == "__main__":
    main()
