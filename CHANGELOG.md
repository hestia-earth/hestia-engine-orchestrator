# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.6.20](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.6.19...v0.6.20) (2025-01-17)


### Features

* **models:** handle filtering with multiple `stage` ([8603606](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/8603606e686856f4423220d5dcd77612972ca667))

### [0.6.19](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.6.18...v0.6.19) (2024-12-10)


### Bug Fixes

* **emissions deleted:** use system boundary utils ([e3a51ed](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/e3a51edb3a61480402b65ae378c324178f515fa1))

### [0.6.18](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.6.17...v0.6.18) (2024-10-01)


### Bug Fixes

* **models:** create deep copy before running in serie ([c813cbc](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/c813cbcbcd008cf186c77be7fa16227610ad0bc4))

### [0.6.17](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.6.16...v0.6.17) (2024-09-03)


### Bug Fixes

* **transformations:** ignore stage when running models for `Transformation` ([112c9ed](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/112c9ed70233174521762d707e8ea9ebb32b8d8b))

### [0.6.16](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.6.15...v0.6.16) (2024-09-03)


### Bug Fixes

* **transformations:** ignore stage when running models for `Transformation` ([901f65e](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/901f65eed28a574feed0b8bc4b554a0d1538d173))

### [0.6.15](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.6.14...v0.6.15) (2024-08-14)


### Features

* **models:** handle exception while running model in parallel ([1d10ea9](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/1d10ea9f27acd2b769181df83c250ff4728f3d0d))


### Bug Fixes

* sip add updated fields on `Term` ([a97b852](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/a97b8520f6d0713128aa5aae7942e2573ab5dd94))

### [0.6.14](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.6.13...v0.6.14) (2024-06-27)


### Bug Fixes

* **merge node:** merge when recalculated value is `0` ([39b4422](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/39b44224a62937833f21e867cc71e4c9edd286b4))

### [0.6.13](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.6.12...v0.6.13) (2024-06-06)


### Bug Fixes

* **transformation:** use Cycle `completeness` when available ([767681f](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/767681fcd7270efb8ae52821baa0c8a5cdca6700)), closes [#27](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/issues/27)

### [0.6.12](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.6.11...v0.6.12) (2024-03-13)


### Bug Fixes

* **merge list:** simplify and match elements based on identical unique properties ([c73e42e](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/c73e42e6a0844b6d21cee2e64bafff31f53769d1))

### [0.6.11](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.6.10...v0.6.11) (2024-03-07)


### Bug Fixes

* **strategies merge:** handle `impactAssessment.[@id](https://gitlab.com/id)` unique keys ([b258552](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/b258552c26c0329825517347c903cca78b78ce5c))

### [0.6.10](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.6.9...v0.6.10) (2024-02-29)


### Features

* **config:** add `config_max_stage` function ([f52deab](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/f52deab1c4030bf2fe5d4772c2d6a61b41b9fa80))


### Bug Fixes

* do not add empty `added` and `updated` arrays ([e9c5216](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/e9c5216fe2695ffc9e3ec4f388049c238ea0c7c8))
* **merge list:** fix merging of blank node using `properties` for uniqueness ([88c67be](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/88c67bea6cb74dfd802efec5fc75140f7c889839))

### [0.6.9](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.6.8...v0.6.9) (2024-02-19)


### Features

* **run:** add `skipAggregated` param to add blank node strategy ([b67c34c](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/b67c34ca9ca90bd58c9e0080e5030fe3046a1070))

### [0.6.8](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.6.7...v0.6.8) (2024-02-16)


### Bug Fixes

* **merge list:** fix replace data at wrong index ([c3d9e15](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/c3d9e150b1c05aa2b2e7ebd2373ee516e9b14d92))

### [0.6.7](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.6.6...v0.6.7) (2024-02-16)


### Bug Fixes

* **merge list:** make sure `term.[@id](https://gitlab.com/id)` always matches to replace element ([fe4df5c](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/fe4df5c09598cdf61ec0d3b96510fb50aa71b4fd))

### [0.6.6](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.6.5...v0.6.6) (2024-02-15)


### Bug Fixes

* **merge list:** make sure `term.[@id](https://gitlab.com/id)` must match ([b0d569e](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/b0d569eb718ae0686463df59dce26f54b7389f77))

### [0.6.5](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.6.4...v0.6.5) (2024-02-15)


### Bug Fixes

* **merge list:** fix same methodModel error ([ce566f3](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/ce566f3d78006289c2543c7d20b83ea7b7b405cf))

### [0.6.4](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.6.3...v0.6.4) (2024-02-12)


### Bug Fixes

* **merge list:** fix merging with identical properties ([cd4ef9f](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/cd4ef9f0394556831d3b45c1dcba56a7baeb87c1))

### [0.6.3](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.6.2...v0.6.3) (2024-01-30)


### Bug Fixes

* **merge list:** fix merging with empty fields ([d48ce44](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/d48ce44f0b3d02ed422cdf02d85f3090c0eee560))

### [0.6.2](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.6.1...v0.6.2) (2023-12-04)


### Features

* handle filtering models by `stage` ([d492661](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/d4926610b6c4c5a0ef20a26c1a44e26e8c247b90))

### [0.6.1](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.6.0...v0.6.1) (2023-06-05)


### Bug Fixes

* **setup:** remove requirement on `models` ([12a28c6](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/12a28c6821e9816f5e03da5453b510d9312e55fc))

## [0.6.0](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.5.3...v0.6.0) (2023-06-01)


### ⚠ BREAKING CHANGES

* **requirements:** min schema version is `21.0.0`
* **requirements:** min models version is `0.46.2`

### Features

* **requirements:** update schema to `21.0.0` ([a895b0d](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/a895b0d84158456d6dabb4a6ab7e92068c99c9fa))

### [0.5.3](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.5.2...v0.5.3) (2023-05-12)


### Bug Fixes

* **transformation:** fix error copying empty value from cycle ([e2edea7](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/e2edea7d3c434aaaf75a434e25f3fcdddaa31564))

### [0.5.2](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.5.1...v0.5.2) (2023-05-08)


### Features

* **transformations:** copy `site` and `otherSites` from Cycle if not present ([2409706](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/2409706b412368ccdf54121d4834211a364beaf8))

### [0.5.1](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.5.0...v0.5.1) (2023-03-17)


### Bug Fixes

* **merge list:** use Node type and field to determine uniqueness merging ([169b3b3](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/169b3b3ad1e3b7cd7cf2f2071667c026fa9f4b90))

## [0.5.0](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.4.6...v0.5.0) (2023-03-13)


### ⚠ BREAKING CHANGES

* **merge node:** schema min version `17` required

### Features

* **merge node:** handle `not relevant` methodTier order ([fef9a0d](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/fef9a0d5652d4e9fd47296ee4961eee6426fdb03))

### [0.4.6](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.4.5...v0.4.6) (2023-01-30)


### Features

* **merge list:** add parameter to skip replacing results with same Term ([a61f65e](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/a61f65e51f59fe96490c5c6b4072192678c216b2))

### [0.4.5](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.4.4...v0.4.5) (2022-12-30)


### Bug Fixes

* **log:** handle log node undefined ([04da22b](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/04da22bf5269ece0620f058fc8c04266be582a58))

### [0.4.4](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.4.3...v0.4.4) (2022-12-28)


### Bug Fixes

* **strategies run:** handle running added `Emission` with `0` value ([2e5a7e0](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/2e5a7e02d5dd44a26e8cc02c5597e5541f54097d))

### [0.4.3](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.4.2...v0.4.3) (2022-12-27)


### Bug Fixes

* **merge node:** handle source obj is `None` ([fd9d475](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/fd9d4757138be1c90f806aec1e61466e6516cc26))

### [0.4.2](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.4.1...v0.4.2) (2022-12-21)


### Features

* **strartegies:** log additional details from `args` ([d8896c0](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/d8896c02627211ce98501b0b20412c79ef2a3112))

### [0.4.1](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.4.0...v0.4.1) (2022-11-30)


### Bug Fixes

* **utils:** handle update version of newly added blank nodes ([e90f180](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/e90f180d03df93a65848560ac0a83499de9b9378))

## [0.4.0](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.3.7...v0.4.0) (2022-11-29)


### ⚠ BREAKING CHANGES

* **transformations:** min schema version `14`

### Features

* **transformations:** use `transformedShare` instead of `previousTransformationShare` ([41f30f2](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/41f30f2ff2fcb5e5cc0528ae941e41af3d9a56e1))

### [0.3.7](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.3.6...v0.3.7) (2022-11-24)


### Features

* **strategies:** log `run_required` when checking Node Type ([92e1f59](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/92e1f5993e0846911b0f09681d3296c36f980274))

### [0.3.6](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.3.5...v0.3.6) (2022-10-20)


### Bug Fixes

* **transformations:** do not override first Transformation input value from Cycle ([38dafd2](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/38dafd230105f8a9f8deaecbd8e4a1c06a339d0b))

### [0.3.5](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.3.4...v0.3.5) (2022-10-20)


### Bug Fixes

* **transformations:** override Input `value` from previous Product with same Term ([b9bb8ee](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/b9bb8ee20bceb6ee7c7291de91d5b66398f9f8e2))

### [0.3.4](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.3.3...v0.3.4) (2022-10-19)


### Features

* **transformations:** add missing excreta inputs before running models ([210a5b5](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/210a5b5080522d7898b9ce26d057e64932406864))

### [0.3.3](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.3.2...v0.3.3) (2022-08-17)


### Features

* **strategies run:** skip running models where Node `type` is not allowed ([ee03cb7](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/ee03cb79ed186f3c103c5c098dfc3b00b1410100))
* **utils:** implenent deep version changes detection ([44e699f](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/44e699f1c401277ad5da8aedfea9537d6b3e05a8))

### [0.3.2](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.3.1...v0.3.2) (2022-07-22)


### Features

* **log:** include node info when logging `should_run` ([9c54d62](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/9c54d620d4a58a1118c6a621a60d0af4a232d401))

### [0.3.1](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.3.0...v0.3.1) (2022-07-11)


### Features

* **strategies run:** add `termId` params ([74396d2](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/74396d2de3c670ad83ce93a5be2de53415f76637))

## [0.3.0](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.2.22...v0.3.0) (2022-07-04)


### ⚠ BREAKING CHANGES

* **run:** removed mutliple args from `add_blank_node_if_missing`

### Features

* **merge list:** handle merge by `transformation.[@id](https://gitlab.com/id)` ([acf055e](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/acf055e3880d301e40aac1de0c6d7bcd6b1ca0f4))


* **run:** multiple checks removed from run strategy ([e4d1056](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/e4d1056a61319332e17f9bdd0178fc3072223831))

### [0.2.22](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.2.21...v0.2.22) (2022-04-25)


### Features

* **strategies run:** handle restriction primary product `[@id](https://gitlab.com/id)` ([838b380](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/838b380dbb0fec1877effb1c893797a17621baaf))

### [0.2.21](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.2.20...v0.2.21) (2022-04-23)


### Bug Fixes

* **add blank node if missing:** handle lookup values as lower case ([8ba2a28](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/8ba2a28f1363180c88700eec6c5cdcc5af1c4a9d))

### [0.2.20](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.2.19...v0.2.20) (2022-04-09)


### Bug Fixes

* **strategies run:** default to allow all if lookup value not found ([fd22554](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/fd22554ee2e0f67b2b5e566cab18e63193d2b0f2))

### [0.2.19](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.2.18...v0.2.19) (2022-04-09)


### Bug Fixes

* **strategies run:** run checks on config `value` (term `[@id](https://gitlab.com/id)`) ([74062f8](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/74062f866209a475d5e2b1e5406d77c999a21871))

### [0.2.18](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.2.17...v0.2.18) (2022-04-09)


### Bug Fixes

* **strategies run:** fix checks always running model ([202b7dc](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/202b7dcd4d9d623a1e3f8e4a59f935b17c4e3170))

### [0.2.17](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.2.16...v0.2.17) (2022-04-09)


### Features

* **run:** add `checkProductTermTypeAllowed` parameter ([64d06a1](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/64d06a10f46158588deba0842aaa6d16ac97f159))
* **run:** handle value empty run ([93f35da](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/93f35da5b192d5a868286504abd3adb4085f3cdd))

### [0.2.16](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.2.15...v0.2.16) (2022-03-21)


### Features

* **run:** add `checkSiteTypeAllowed` parameter ([1ffc76c](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/1ffc76cb158ab5b123fc57f8c1438b5cc162f3db))
* **run:** add parameter to skip empty value should run ([f806a27](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/f806a274a2b8ad941ff09adbe1650933b3bd77f3))
* **run:** run model is node is present but has no `value` ([ca48d1e](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/ca48d1e99c8042ba2c6b3a0d451aa87c3831668e))

### [0.2.15](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.2.14...v0.2.15) (2022-02-23)


### Bug Fixes

* **log:** disable steam handler completely ([3fd4573](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/3fd4573768b061e31765df2051dd6fb155b9c8b5))

### [0.2.14](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.2.13...v0.2.14) (2022-02-23)


### Features

* **log:** use `LOG_TO_STREAM` to disable logging to stream (enabled by default) ([64730ca](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/64730caa05e79ef192c6da940b0a842bfb380358))

### [0.2.13](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.2.12...v0.2.13) (2022-02-23)


### Features

* **log:** expose stream handler ([75828af](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/75828af5851a89a3e397df7fba7bb405fe7bef2c))

### [0.2.12](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.2.11...v0.2.12) (2022-02-15)


### Features

* **log:** set file log level to `DEBUG` ([ae3bddb](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/ae3bddbd3b27d3f946444ed7ffb2e929f4e64be4))

### [0.2.11](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.2.10...v0.2.11) (2021-10-19)


### Bug Fixes

* **models transformation:** fix previous value not taken into account ([22df1bd](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/22df1bd3de6323a2ec7f8fd4ed175748d1d288e8))

### [0.2.10](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.2.9...v0.2.10) (2021-10-19)


### Bug Fixes

* **models transformations:** run all transformations regardless of chaining ([19d1286](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/19d128686e60c16e3321d68b44c76bbe4323f6d3))

### [0.2.9](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.2.8...v0.2.9) (2021-10-01)


### Features

* **merge list:** handle merge with `inputs` ([e9296cf](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/e9296cf443f5528f050113221e49d13472399b6c))

### [0.2.8](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.2.7...v0.2.8) (2021-09-25)


### Bug Fixes

* **utils:** skip added/updated fields for versioning ([2a2852f](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/2a2852fd0167d590138bf6b40140c81ce88cf613))

### [0.2.7](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.2.6...v0.2.7) (2021-09-25)


### Features

* **strategies merge append:** handle merge `list` and single elements ([d424b39](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/d424b39341e94bff9b911efda1d9bf643cfd022e))

### [0.2.6](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.2.5...v0.2.6) (2021-09-22)


### Features

* **strategies:** add `append` strategy ([669d324](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/669d3247cea7d267c95757fc0e3b7851fa70ec4b))

### [0.2.5](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.2.4...v0.2.5) (2021-09-09)


### Features

* **models:** add `emissions.deleted` model ([7e5f7d9](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/7e5f7d947a6bd113bb3c938e54cf9dd65c3ccc40))

### [0.2.4](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.2.3...v0.2.4) (2021-09-01)


### Features

* handle non-indexed Nodes ([e0d393b](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/e0d393b92c6b29774c35d59a738a44571d527664))

### [0.2.3](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.2.2...v0.2.3) (2021-08-09)


### Features

* **transformations:** copy some `Practice` from `Cycle` ([d335ae1](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/d335ae1531ec46702209152fd617b1ad47c57304))

### [0.2.2](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.2.1...v0.2.2) (2021-08-09)


### Features

* **transformations:** set `dataCompleteness` to `True` ([c269124](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/c269124060f7b04f0b8c9e38cb1e482c4a5b6bab))

### [0.2.1](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.2.0...v0.2.1) (2021-08-03)


### Bug Fixes

* **model transformations:** fix replace product input not found ([1ed70fc](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/1ed70fcb72a8568a92d4f7c52ee04fc32004c7d1))

## [0.2.0](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.1.3...v0.2.0) (2021-07-26)


### ⚠ BREAKING CHANGES

* **merge list:** use models `0.11.3` working on schema `6.0.0`

### Features

* **merge list:** add param to replace by `methodModel` ([4ef22b4](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/4ef22b446aec6df143987e1a7219052c5f939d88))

### [0.1.3](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.1.2...v0.1.3) (2021-07-21)


### Features

* **models:** add `transformations` ([736e5b3](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/736e5b3b6420982ab68fb6d8796ac62413e9b236))
* **models:** add env variable to limit the number of parallel models ([09fa5df](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/09fa5df3ce91076c6350a9433b4f58922fb9ac70))


### Bug Fixes

* **merge list:** handle `operation` for unique merge keys ([fbbaea5](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/fbbaea5ab8a427a0ef7d17385dd25c8cebe8b346))
* **transformations:** set the `term` as first practice ([11be0e3](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/11be0e3fd86a6e2b3d68037ef63896635168ebca))

### [0.1.2](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.1.1...v0.1.2) (2021-06-12)


### Bug Fixes

* **strategies:** handle merge with same tier but high threshold ([4406a02](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/4406a020345d40e4f48ff3d57d5f563b77d0b69e))
* **strategies:** skip merge if new value is `0` ([8c59f82](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/8c59f8296126c75c3dae6ad289944ca812426b1f))

### [0.1.1](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.1.0...v0.1.1) (2021-05-21)


### Bug Fixes

* **log:** set LEVEL from env for file logging ([6f65efa](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/6f65efac42db07d8d1615390db5fbc702b32054a))
* **merge node:** handle no source ([a4fab3b](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/a4fab3bcb56328854edd1f9fa0ac0366ae816dd1))

## [0.1.0](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/compare/v0.0.1...v0.1.0) (2021-05-18)


### Bug Fixes

* **merge:** handle skip replace lower tier ([6636b6a](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/6636b6a4580629790256c0886cba2ce667a3b029))

### 0.0.1 (2021-05-13)


### Features

* **config:** add `ImpactAssessment` config ([c56f884](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/c56f884c9eb2dbda88f5b18553369d621613b723))
* **config:** add Cycle configuration ([8761fc2](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/8761fc2b4ba98f0af92981dc74f3ed4cb11ad43a))
* **orchestrator:** handle site configuration ([177f394](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/177f394c03131d1b1b3d81c685632eb29611242b))
* **package:** use schema 3.3.0 ([713571b](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/713571b30f1f6f570dfe5809fbade2270068041e))
* **strategies merge:** handle data type using config ([fb35d67](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/fb35d673c6cc389b87e15f349668e63ab3b33858))
* **strategies merge:** handle replace value with threshold ([abf99b0](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/abf99b07d173c48d86bb027bdffd836ff3323bef))
* **strategies merge:** update node added/updated arrays ([75b4b57](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/75b4b57a7017bee4b500ca289a2a22d68bf7a99a))
* **strategies run:** handle run with already added term ([85f4362](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/85f4362e62a7cd8f9d48966498f06b297cf08a0b))
* **strategies run:** handle skip `methodTier` = `measured` on emissions ([c9f88c3](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/c9f88c3bc2d3d47f6b084ba930f68015e4fc901a))


### Bug Fixes

* **merge:** fix merging of dictionaries ([fe7e95d](https://gitlab.com/hestia-earth/hestia-engine-orchestrator/commit/fe7e95d78a899d5dc50a2b3f7aa7975273af5cb3))
