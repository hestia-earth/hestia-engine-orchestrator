#!/bin/sh

docker build --progress=plain \
  -t hestia-engine-orchestrator:test \
  -f tests/Dockerfile \
  .

docker run --rm \
  -v ${PWD}/coverage:/app/coverage \
  -v ${PWD}/hestia_earth:/app/hestia_earth \
  -v ${PWD}/tests:/app/tests \
  hestia-engine-orchestrator:test "$@"
