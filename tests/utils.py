import os

# use `FORCE=true pytest ...` to overwirte result file
overwrite_results = os.getenv('FORCE', 'false') == 'true'
fixtures_path = os.path.abspath('tests/fixtures')


def order_lists(obj: dict, props: list):
    for prop in props:
        if prop in obj:
            obj[prop] = sorted(obj[prop], key=lambda x: x.get('term').get('@id'))
