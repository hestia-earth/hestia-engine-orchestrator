# Run strategy > Add blank node if missing

Run the model if the Blank Node is missing in the list identified by the `key`.

Only used for Blank Node like `Emission`, `Input`, `Product`, etc.

## Arguments

This strategy accepts additional arguments via `runArgs`:

| key | format | default | description |
| ------ | ------ | ------ | ------ |
| `skipEmptyValue` | `boolean` | `false` | If set to `true`, Blank Node without `value` will not be calculated. |
| `skipAggregated` | `boolean` | `false` | If set to `true`, will not run on `aggregated` Node |
| `termId` | `string` | `None` | Replace `key` as identifier for existing Blank Node. |
| `runNonAddedTerm` | `boolean` | `false` | If set to `true`, existing Blank Node without `added: ["term"]` will be calculated. Example: `tier 1` model that has a corresponding `tier 2` model should **not** run if `tier 2` model set the value |
| `runNonMeasured` | `boolean` | `false` | If set to `true`, existing Blank Node with `methodTier` != `measured` will be calculated. |
