# Run strategy > Add key if missing

Run the model only if the value at `key` is not present.

Does **not** work on Blank Nodes that will be merged into a list.
