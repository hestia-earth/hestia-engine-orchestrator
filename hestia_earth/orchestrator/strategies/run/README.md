# Run Strategies

This strategy determines if the model will be run at all.

Here are the possible running strategies:

| value | documentation |
| ------ | ------ |
| `always` | [view here](./always.md) |
| `add_blank_node_if_missing` | [view here](./add_blank_node_if_missing.md) |
| `add_key_if_missing` | [view here](./add_key_if_missing.md) |
