# Merge strategy > List

Merge 2 different lists.

If both lists contains Blank Node, [node strategy](./merge_node.md) will be used for every Blank Node.

## Arguments

This strategy accepts additional arguments via `mergeArgs`:

| key | format | default | description |
| ------ | ------ | ------ | ------ |
| `sameMethodModel` | `boolean` | `false` | By default, merging is based on a few identical keys like `term.@id`, and using a different `methodModel` will still override existing values. Set to `true` to only replace value for matching `methodModel`. |
| `skipSameTerm` | `boolean` | `false` | Skip replacing a Blank Node if it has the same `term.@id`. Will take into account unique keys to identify which Blank Node to skip. |
