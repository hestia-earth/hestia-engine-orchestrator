# Merge strategy > Node

Merge 2 different nodes.

## Arguments

This strategy accepts additional arguments via `mergeArgs`:

| key | format | default | description |
| ------ | ------ | ------ | ------ |
| `replaceThreshold` | `[str, float]` | `[None, 0]` | Only replaces the value assigned to the key (first arg) if the different is more than the threshold. Example: `["value", 0.01]` will only replace the Node if it's calculated `value` is more than 1% different from the original `value`. |
| `replaceLowerTier` | `boolean` | `false` | Allows to replace values which have a lower or identical `methodTier`. If set to `false`, only higher `methodTier` will replace the value if already present. |
