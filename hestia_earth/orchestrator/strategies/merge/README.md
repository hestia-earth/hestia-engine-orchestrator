# Merge Strategies

This strategy determines how the result will be merged into the original data.

Here are the possible merging strategies:

| value | documentation |
| ------ | ------ |
| `default` | [view here](./merge_default.md) |
| `node` | [view here](./merge_node.md) |
| `list` | [view here](./merge_list.md) |
