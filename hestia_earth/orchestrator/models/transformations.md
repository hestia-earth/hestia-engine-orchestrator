# Transformations

This model runs a list of models for each Cycle [transformations](https://hestia.earth/schema/Cycle#transformations).

The following fiels from the Cycle are copied to the Transformation before running the models:
- [site](https://hestia.earth/schema/Cycle#site)
- [cycleDuration](https://hestia.earth/schema/Cycle#cycleDuration): only if [transformationDuration](https://hestia.earth/schema/Transformation#transformationDuration) has not been provided
- [functionalUnit](https://hestia.earth/schema/Cycle#functionalUnit)
- [startDate](https://hestia.earth/schema/Cycle#startDate): only if [startDate](https://hestia.earth/schema/Transformation#startDate) has not been provided
- [endDate](https://hestia.earth/schema/Cycle#endDate): only if [endDate](https://hestia.earth/schema/Transformation#endDate) has not been provided


Addiotionally, the [transformation.term](https://hestia.earth/schema/Transformation#term) is converted into a [Practice](https://hestia.earth/schema/Practice) and inserted at the beginning of the [practices](https://hestia.earth/schema/Transformation#practices) list.
