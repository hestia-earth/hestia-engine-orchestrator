# Emissions Deleted

This model will mark as [deleted](https://hestia.earth/schema/Emission#deleted) all the original emissions that are not
in HESTIA default system boundary.
