# Hestia Engine Orchestrator

Orchestrate your different models to run on a Cycle, an ImpactAssessment or a Site.

## Install

1. Install the library:
```bash
pip install hestia_earth.orchestrator
```

You can now install your own models or follow the steps below to use the default Hestia models.

### Install the Hestia Models

If you want to use the [hestia default models](/hestia-earth/hestia-engine-models), follow these steps:

1. Install the library:
```bash
pip install hestia_earth_models
```
2. Install the config library:
```bash
pip install hestia_earth_config
```

### Using your own models

You can create your own models in addition (or instead of) the default set of models provided by Hestia.

The model needs to expose only one method:
```python
def run(key: str, data): ...
```
It will be given the data that has been given to the orchestrator, i.e. by calling:
```python
from hestia_earth.orchestrator import run

my_data = {'@type': 'Cycle', 'inputs': []}
config = {
  "models": [{
    "key": "inputs",
    "model": "my_model",
    "value": "my_model_value",
    "runStrategy": "add_if_missing_key"
  }]
}
run(my_data, config)
```
Will be calling in your own model `my_model.py`:
```python
def run('my_model_value', my_data: dict): ...
```

### Usage

```python
# will work with either Cycle or Site
from hestia_earth.orchestrator import run
from hestia_earth.config import load_config

# cycle is a JSONLD node cycle
cycle = {'@type': 'Cycle', ...}
result = run(cycle, load_config(cycle))
print(result)
```
