#!/bin/bash

# exit when any command fails
set -e

STAGE=${1:-"dev"}
AWS_REGION=${REGION:=us-east-1}
PKG_PATH=./python/lib/python3.9/site-packages

cd ./layer/

rm -rf layer.zip
zip -r layer.zip python

aws lambda publish-layer-version \
    --region $AWS_REGION \
    --layer-name "hestia-$STAGE-python39-engine-orchestrator" \
    --description "Engine Orchestrator running on python 3.9" \
    --zip-file "fileb://layer.zip" \
    --compatible-runtimes python3.9
