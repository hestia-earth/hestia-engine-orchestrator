#!/bin/sh

chmod +x scripts/download_config.sh && ./scripts/download_config.sh

docker build --progress=plain \
  -t hestia-engine-orchestrator:latest \
  .

docker run --rm \
  --name hestia-engine-orchestrator \
  -v ${PWD}:/app \
  hestia-engine-orchestrator:latest python run.py "$@"
