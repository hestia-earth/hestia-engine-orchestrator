import sys
import json
from hestia_earth.orchestrator.models.transformations import (
    _next_transformation, _apply_transformation_share, _convert_transformation
)


def _run_serie(cycle: dict, previous: dict, transformations: list):
    transformation = _next_transformation(previous, transformations)
    transformation = _apply_transformation_share(previous, transformation) if transformation else None
    result = _convert_transformation(cycle, transformation) if transformation else None
    # if no next transformation, stop the loop
    return ([result] + _run_serie(cycle, result, transformations)) if transformation else []


def _save_transformation(filename: str):
    def run(values: tuple):
        index, value = values
        with open(f"{filename}-transformation-{str(index)}.jsonld", 'w') as f:
            f.write(json.dumps(value, indent=2, ensure_ascii=False))
    return run


def main(args: list):
    filepath = args[0]
    with open(filepath) as f:
        cycle = json.load(f)

    transformations = cycle.get('transformations', [])
    data = _run_serie(cycle, cycle, transformations)
    return list(map(_save_transformation(filepath.replace('.jsonld', '')), enumerate(data)))


if __name__ == "__main__":
    main(sys.argv[1:])
